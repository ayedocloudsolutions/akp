## [1.11.1](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.11.0...v1.11.1) (2021-08-23)


### Bug Fixes

* corrected cert-manager annotations ([910a588](https://gitlab.com/ayedocloudsolutions/akp/commit/910a588608c9d3ac22f591e4206d4d879db4b8a5))

# [1.11.0](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.10.5...v1.11.0) (2021-08-23)


### Bug Fixes

* add to defaults ([17d2c18](https://gitlab.com/ayedocloudsolutions/akp/commit/17d2c18a6689b795070f6213156fc36fe1006ed6))
* add to defaults ([4c58efa](https://gitlab.com/ayedocloudsolutions/akp/commit/4c58efa9a6ddd16c9916f70b498846e675b5b70a))
* comment version ([61bc995](https://gitlab.com/ayedocloudsolutions/akp/commit/61bc995608cf9c4fb4126f66d9d833e40708828b))
* comment version ([53d3e40](https://gitlab.com/ayedocloudsolutions/akp/commit/53d3e408d899d265cbdd68db77d5de34e3694b00))
* install error in install keycloak ([a04094d](https://gitlab.com/ayedocloudsolutions/akp/commit/a04094d8b43c7d0de971b6a222c21f84960fd4bb))
* install error in install keycloak ([b375e85](https://gitlab.com/ayedocloudsolutions/akp/commit/b375e856cd59a1c329f84ce8154b87ca75ff26e4))
* merge ([8039986](https://gitlab.com/ayedocloudsolutions/akp/commit/8039986f851f97265a83e67a8daf25dd7ff5d629))
* merge ([a7115ce](https://gitlab.com/ayedocloudsolutions/akp/commit/a7115cef5d0abdda7262aefe48bd6e6849a8a4e5))
* Merge issues ([8af6386](https://gitlab.com/ayedocloudsolutions/akp/commit/8af63868fa701c82aae479dfe4122c0104904491))
* Merge issues ([492fd2b](https://gitlab.com/ayedocloudsolutions/akp/commit/492fd2b5aa0bcc2fbd107db42a354dfa3a187221))
* Merge issues ([5bb25ad](https://gitlab.com/ayedocloudsolutions/akp/commit/5bb25adda30a824660e81aeb0d08aad9522e9a9a))
* test domain deleted ([57e4368](https://gitlab.com/ayedocloudsolutions/akp/commit/57e43682279b3e0b6e920fb5e1eac762a9b88871))


### Features

* add to install.yml ([896e6eb](https://gitlab.com/ayedocloudsolutions/akp/commit/896e6eb5d26dbe7b72175c8619766e4d9401ce29))
* instal routine ([90d86f3](https://gitlab.com/ayedocloudsolutions/akp/commit/90d86f318ae0deac1e06b89506fe1448306853ee))
* instal routine ([e7a2f58](https://gitlab.com/ayedocloudsolutions/akp/commit/e7a2f58f885cfcd8e6237bc120c700b338aab679))

## [1.10.5](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.10.4...v1.10.5) (2021-08-23)


### Bug Fixes

* wrong var ([4f1322c](https://gitlab.com/ayedocloudsolutions/akp/commit/4f1322cfbc0127c5ac1b22778db2c754af0a3907))

## [1.10.4](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.10.3...v1.10.4) (2021-08-23)


### Bug Fixes

* incorrect variable used ([63dc445](https://gitlab.com/ayedocloudsolutions/akp/commit/63dc4455732c2a2531a30ce86491f7f723273996))

## [1.10.3](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.10.2...v1.10.3) (2021-08-19)


### Bug Fixes

* execution order ([244a82f](https://gitlab.com/ayedocloudsolutions/akp/commit/244a82f4328e6f4f214513b35fd7d4e9fd38121a))
* removed linkerd from internal services ([9e2671f](https://gitlab.com/ayedocloudsolutions/akp/commit/9e2671f74059c4b33e952881bee68b864df03819))

## [1.10.2](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.10.1...v1.10.2) (2021-08-19)


### Bug Fixes

* execution order ([90754b9](https://gitlab.com/ayedocloudsolutions/akp/commit/90754b9dd2bfa2122991e550dfe705f03c09789e))

## [1.10.1](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.10.0...v1.10.1) (2021-08-19)


### Bug Fixes

* refactored Prometheus to templated helm vars ([cec143c](https://gitlab.com/ayedocloudsolutions/akp/commit/cec143c44e03df6e2673da5a92ea1bbfd2ba247e))

# [1.10.0](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.9.0...v1.10.0) (2021-08-18)


### Features

* added argocd ingress ([434c736](https://gitlab.com/ayedocloudsolutions/akp/commit/434c7368ce3451b796d3cae86dd0c715665bc926))

# [1.9.0](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.8.0...v1.9.0) (2021-08-18)


### Features

* added argocd ingress ([fc1cfbc](https://gitlab.com/ayedocloudsolutions/akp/commit/fc1cfbcd3532e9c5ab3b0a60c10997286884d296))

# [1.8.0](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.7.2...v1.8.0) (2021-08-18)


### Bug Fixes

* enterprise switch ([b4ec2b4](https://gitlab.com/ayedocloudsolutions/akp/commit/b4ec2b485371dfb8ffd161a2c2d24af9301cc229))


### Features

* added argocd ingress ([fdf62a9](https://gitlab.com/ayedocloudsolutions/akp/commit/fdf62a96f4d37939329effdf242951c778739c71))
* Portainer with ingress and Letsencrypt ([52b4ec3](https://gitlab.com/ayedocloudsolutions/akp/commit/52b4ec3f90c96b87cb4ba501c2d930cfa30b59f3))

## [1.7.2](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.7.1...v1.7.2) (2021-08-12)


### Bug Fixes

* added missing config ([dddf393](https://gitlab.com/ayedocloudsolutions/akp/commit/dddf393ea8554d89cb4a91635daf130e481c6070))

## [1.7.1](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.7.0...v1.7.1) (2021-08-12)


### Bug Fixes

* ssh usage ([2199977](https://gitlab.com/ayedocloudsolutions/akp/commit/21999775626fc598f4137c9454459764e11bd363))

# [1.7.0](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.6.0...v1.7.0) (2021-08-12)


### Bug Fixes

* default disabled ([73fb07d](https://gitlab.com/ayedocloudsolutions/akp/commit/73fb07d3d9d8949096e1d7da58104d10b3d1e485))


### Features

* added Portainer to AKP ([1eb17bf](https://gitlab.com/ayedocloudsolutions/akp/commit/1eb17bf009bbb060750625975463d08adb7a21da))

# [1.6.0](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.5.1...v1.6.0) (2021-08-12)


### Features

* docker ([2d6135f](https://gitlab.com/ayedocloudsolutions/akp/commit/2d6135f8cedbc8df9847615f0c873a7e04b94918))

## [1.5.1](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.5.0...v1.5.1) (2021-08-11)


### Bug Fixes

* removed mkdocs.yml ([e63d372](https://gitlab.com/ayedocloudsolutions/akp/commit/e63d372965cf433c932c07ce4f486780aac74410))

# [1.5.0](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.4.2...v1.5.0) (2021-08-11)


### Bug Fixes

* stack name check ([5a44338](https://gitlab.com/ayedocloudsolutions/akp/commit/5a44338263249dd02ab833ae0c1735b64bf1dbf9))


### Features

* added Earthfile ([12f7e24](https://gitlab.com/ayedocloudsolutions/akp/commit/12f7e24aa0bba765ce2f89fa61bc3d1931f15a66))

## [1.4.2](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.4.1...v1.4.2) (2021-08-05)


### Bug Fixes

* corrected pipeline artifacts ([ca86f54](https://gitlab.com/ayedocloudsolutions/akp/commit/ca86f54dd3da779ec70cd8a8e2113ab77944bc99))

## [1.4.1](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.4.0...v1.4.1) (2021-08-05)


### Bug Fixes

* removed release_name from LinkerD task ([8b02efb](https://gitlab.com/ayedocloudsolutions/akp/commit/8b02efb1c4e5aac2defe73c4265a59a08d1694ff))

# [1.4.0](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.3.0...v1.4.0) (2021-08-03)


### Features

* refactored to new config mechanism ([d978f49](https://gitlab.com/ayedocloudsolutions/akp/commit/d978f494bd6682b6cd45977501628187737c7ee1))

# [1.3.0](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.2.2...v1.3.0) (2021-07-12)


### Bug Fixes

* prod_issuer ([12ed067](https://gitlab.com/ayedocloudsolutions/akp/commit/12ed0670875663e26002e202897c617e918091bc))


### Features

* added proposal to use Flux as main deployment strategy for AKP ([57a774f](https://gitlab.com/ayedocloudsolutions/akp/commit/57a774fbfaebf1542e1f3c3771cc04accac197ad))

## [1.2.2](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.2.1...v1.2.2) (2021-07-08)


### Bug Fixes

* mail ([d2eae83](https://gitlab.com/ayedocloudsolutions/akp/commit/d2eae83bfd531c3a373b223da6c174042e4e11c6))

## [1.2.1](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.2.0...v1.2.1) (2021-07-07)


### Bug Fixes

* edit missing quote to certmanger role ([458b36f](https://gitlab.com/ayedocloudsolutions/akp/commit/458b36f3e3c6f069c9fc9330e36f39ca89f20419))

# [1.2.0](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.1.1...v1.2.0) (2021-06-25)


### Features

* added diagram ([c449633](https://gitlab.com/ayedocloudsolutions/akp/commit/c4496339a9ed77e45edca29dec48cc98ddf068a6))

## [1.1.1](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.1.0...v1.1.1) (2021-06-13)


### Bug Fixes

* change base image ([daad792](https://gitlab.com/ayedocloudsolutions/akp/commit/daad79261068a1d220ed52dba41de8c34e158bc3))

# [1.1.0](https://gitlab.com/ayedocloudsolutions/akp/compare/v1.0.0...v1.1.0) (2021-06-13)


### Bug Fixes

* refactored configuration, renamed output_dir to release_dir ([d946917](https://gitlab.com/ayedocloudsolutions/akp/commit/d946917f7195908ca2907bf53cc1522f06f79bab))


### Features

* added Fission as app ([b22c433](https://gitlab.com/ayedocloudsolutions/akp/commit/b22c4333424b6191f6d8a6e99686303e0268563f))
* added Prometheus ServiceMonitors ([2557d0b](https://gitlab.com/ayedocloudsolutions/akp/commit/2557d0b690d088b81b61abc1e9f92d91edd8fcfa))
* added Sonarqube as app ([12aa1c7](https://gitlab.com/ayedocloudsolutions/akp/commit/12aa1c768346605c527e86f3806c9c16bc840af7))

# 1.0.0 (2021-05-27)


### Bug Fixes

* added main as release branch ([0aacedf](https://gitlab.com/ayedocloudsolutions/akp/commit/0aacedfcfd000639eda47ed13e7784b072228517))


### Features

* Launch ayedo Kubernetes Platform ([027eb4d](https://gitlab.com/ayedocloudsolutions/akp/commit/027eb4dd656fe2539b74bc62b10007ab52c87242))
