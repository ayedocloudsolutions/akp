# Implement Flux CD

- [Flux](https://fluxcd.io) is a GitOps based Continuous Delivery tool
- It syncs with a Git repository to sync the state of manifests from the repository to the cluster
- Flux works like ArgoCD, but has no Web UI

## Idea

- Current: we use "helm" with Ansible to directly deploy applications with provided user values to a cluster
- New: we use Ansible to template the necessary Flux manifests, save them $release_dir and push them to the repository linked with the $release_dir where Flux picks them up and syncs them to the cluster it's installed in

## Changes

- things like kubeconfig, inventory.yml, Terraform code, Terraform state and the aforementioned manifests will be called "artifacts"
- "artifacts" will be saved to $release_dir/akp/.artifacts, the so called "artifacts_dir"
- vars.yml is "configuration"
- "configuration" will be saved to $release_dir/akp, the so called "config_dir"
- the $release_dir (a normal directory) will be pushed to a dedicated git repository
- new configuration flags for AKP:
  - ALL APPS: $app.values: contains HELM values to parametrize the installation
    - $app.values will be transparently used by Flux/HELM in Ansible
  - flux.enabled: False
    - installs flux
  - deployment.strategy: helm
    - defaults to "helm", accepts "flux"
    - when set to "flux":
      - instead of installing applications directly via helm, templates for Flux for every application will be generated and saved to the $artifacts_dir
      - We will use Flux' "Helm Controller" so we still use helm charts to deploy applications, we just let Flux handle the actual task of applying the resources
- this repository will be synchronised with the previously installed Flux
  - Flux will deploy applications specified in the AKP's $artifacts_dir automatically when the repository updates
- a new directory called "apps" will be created alongside aki, ake and akp
  - this directory can hold user-provided manifests that will also be automatically deployed by Flux when the repository updates
- OPTIONAL: a new directory called ".ssh"
  - contains dedicated SSH credentials for this release
  - will serve as an input for AKI + AKE

### New directory layout of $release_dir

$release_name: demo

```bash
demo/
├── ake/
│   ├── .artifacts/
│   │   ├── kubeconfig.yml
│   ├── vars.yml
├── aki/
│   ├── .artifacts/
│   │   ├── inventory.yml
│   │   ├── Terraform.tfstate
│   │   ├── main.tf
│   ├── vars.yml
├── akp/
│   ├── .artifacts/
│   │   ├── flux-system/
│   │   ├── ingress-nginx/
│   │   |   ├── app.yml
│   │   |   ├── kustomization.yml
│   │   ├── prometheus/
│   ├── vars.yml
└── apps/
    ├── .artifacts/
    │   ├── my-app/
    │   |   ├── app.yml
    │   |   ├── kustomization.yml
    │   ├── my-app-2/
    │   |   ├── app.yml
    │   |   ├── kustomization.yml
    ├── vars.yml
```

## Benefits

We're experimenting with CD (Argo) for a while now and it's a generally good approach. Argo is very UI-centric (or CLI-centric) but the thing we care most about is stability, predictability and automation, which makes flux with its simplicity and GitOps-first approach a better option.

Using it to deploy both, user applications AND the platform components, has a few benefits:

- we have an automated mechanism for deployment of all relevant Kubernetes components
- auto-updates are now possible with interacting with the repository
- having dedicated .artifacts directories per module enables a better workflow when invoking more than 1 of the modules (you can build a input-output chain - output (artifact) of module 1 (AKI) will be input of module 2 (AKE))
- the more we do with Git and Pipelines, the better we are prepared for going 100% automation
- Flux is very lightweight, failsafe and has many options geared towards the safety of deployments (that HELM doesn't have)
